
## Cara Instalasi


- Buat folder **bowirta** pada folder htdocs anda
- Masukkan file aplikasi ini ke dalamnya
- Jalankan **composer install** atau **composer update**
- Setelah selesai,buat database dan beri nama db_bowirta di phpmyadmin anda
- Kemudian import file **db_bowirta.sql**  yang ada di folder **db** ke phpmyadmin
- Setting **.env** anda sesuai dengan konfigurasi phpmyadmin anda
- Dan setting **APP_URL** sesuai nama aplikasinya
- Akses Aplikasi langsung dengan localhost/namaaplikasi

## Untuk Testing
- username : **admin**
- password : **admin**

Sekian dan terima kasih


