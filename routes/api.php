<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'barang'], function () {
    Route::get('/get-all', 'Api\BarangController@getAll');
    Route::get('/get-one/{id}', 'Api\BarangController@getOne');
    Route::post('/create', 'Api\BarangController@create');
    Route::post('/update/{id}', 'Api\BarangController@update');
    Route::delete('/delete/{id}', 'Api\BarangController@delete');
});
