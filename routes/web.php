<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\BackendController@index')->middleware('verify-login');
Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware' => ['verify-login']], function () {
    Route::get('/', 'BackendController@index');
    Route::get('/menu','BackendController@menu');
    Route::group(['prefix' => 'master', 'namespace' => 'Master','middleware' => ['verify-login','verify-menu']],function (){


        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@index');
            Route::post('/add', 'RoleController@form');
            Route::post('/save', 'RoleController@save');
            Route::post('/delete', 'RoleController@delete');
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@index');
            Route::post('/add', 'UserController@form');
            Route::post('/save', 'UserController@save');
            Route::post('/delete', 'UserController@delete');
        });

        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index');
            Route::post('/add', 'PelangganController@form');
            Route::post('/save', 'PelangganController@save');
            Route::post('/delete', 'PelangganController@delete');
        });

        Route::group(['prefix' => 'supplier'], function () {
            Route::get('/', 'SupplierController@index');
            Route::post('/add', 'SupplierController@form');
            Route::post('/save', 'SupplierController@save');
            Route::post('/delete', 'SupplierController@delete');
        });

        Route::group(['prefix' => 'barang'], function () {
            Route::get('/', 'BarangController@index');
            Route::get('/get-data', 'BarangController@getData');
            Route::post('/add', 'BarangController@form');
            Route::post('/save', 'BarangController@save');
            Route::post('/delete', 'BarangController@delete');
        });


    });

    Route::group(['prefix' => 'transaksi', 'namespace' => 'Transaksi','middleware' => 'verify-login'],function (){

        Route::group(['prefix' => 'pembelian'], function () {
            Route::get('/', 'PembelianController@index');
            Route::get('/add', 'PembelianController@form');
            Route::post('/save', 'PembelianController@save');
            Route::post('/delete', 'PembelianController@delete');

        });

        Route::group(['prefix' => 'penjualan'], function () {
            Route::get('/', 'PenjualanController@index');
            Route::get('/add', 'PenjualanController@form');
            Route::post('/save', 'PenjualanController@save');
            Route::post('/delete', 'PenjualanController@delete');

        });



    });
    Route::group(['prefix' => 'report', 'namespace' => 'Report','middleware' => 'verify-login'],function (){

        Route::group(['prefix' => 'mutasi-stock'], function () {
            Route::get('/', 'MutasiStockController@index');
            Route::post('/show', 'MutasiStockController@show');

        });





    });

});