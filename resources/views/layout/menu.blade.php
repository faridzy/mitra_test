
<li>
    <a href="{{url('/backend')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
</li>

{!! Session::get('menu')!!}

<li>
    <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Transaksi</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        @if(strtoupper(Session::get('activeUser')->getRole->nama_role)=='ADMINISTRATOR')
        <li><a href="{{url('/backend/transaksi/pembelian')}}">Pembelian</a></li>
        <li><a href="{{url('/backend/transaksi/penjualan')}}">Penjualan</a></li>
        @endif
        @if(strtoupper(Session::get('activeUser')->getRole->nama_role)=='LOGISTIK')
                <li><a href="{{url('/backend/transaksi/pembelian')}}">Pembelian</a></li>
        @endif

            @if(strtoupper(Session::get('activeUser')->getRole->nama_role)=='FAKTURIS')
                <li><a href="{{url('/backend/transaksi/penjualan')}}">Penjualan</a></li>
            @endif



    </ul>
</li>

<li>
    <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Mutasi</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{url('/backend/report/mutasi-stock')}}">Mutasi Stok</a></li>


    </ul>
</li>
<li>
    <a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
</li>