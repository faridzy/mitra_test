@extends('layout.main')
@section('title', $title)

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{$title}}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>
                <li>
                    <a href="#">Transaksi</a>
                </li>
                <li>
                    <a href="#">Pembelian</a>
                </li>
                <li>
                    <a href="#">{{$title}}</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row wrapper border-bottom white-bg wrapper-content animated fadeInRight" style="min-height: 600px;">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div id="result-form-konten"></div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="pull-left">
                                    <a href="{{url('/backend/transaksi/pembelian')}}" type="button" class="btn btn-default button"><i class="fa fa-reply fa-5px"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
                            <div class='form-group'>
                                <label for='warehouse_id' class='col-sm-2 col-xs-12 control-label'>Supplier</label>
                                <div class='col-sm-10 col-xs-12'>
                                    <select name="supplier_id" class="form-control" id="supplier_id">
                                        @foreach($supplier as $num => $item)
                                            <option value="{{$item->id}}" @if(!is_null($data)) @if($item->id == $data->supplier_id) selected="selected" @endif @endif >{{$item->nama_supplier}}</option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>
                            <div class='form-group'>
                                <label class="col-sm-2 control-label">Tanggal Pembelian</label>
                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="tanggal_pembelian" class="form-control" required>
                                    </div>
                                </div>

                            </div>

                            <div class='form-group'>
                                <label class="col-sm-2 control-label">Total Harga</label>
                                <div class="col-sm-10">
                                   <input type="text" id="total" name="total_biaya" class="form-control" readonly>
                                </div>

                            </div>

                            <table class="table table-striped table-bordered table-hover"  id="dynamic_field">
                                <thead>
                                <tr>
                                    <th width="20%">Kode Barang</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>SubTotal</th>
                                    <th><button type="button" name="add" id="add" class="btn btn-success">+</button></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <div class='form-group'>
                                <label class='col-sm-4 col-xs-12 control-label'></label>
                                <div class='col-sm-3 col-xs-12'>
                                    <input type="submit" class="btn btn-primary" value="Save">

                                    <a onclick="backToParent()" class="btn btn-danger">Back</a>
                                </div>
                            </div>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type='hidden' name='id' value='{{ $data->id }}'>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/transaksi/pembelian/save', data, '#result-form-konten');
        });

        function pindah()
        {
            redirect(200, "/backend/transaksi/pembelian");
        }

        function backToParent() {
            modalConfirm("Confirmation", "Are you sure want to Back ?", function () {
                redirect(200, "/backend/transaksi/pembelian");
            })
        }
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            startDate: "today",
            format: "yyyy-mm-dd"
        })







        $(document).ready(function() {
            var i = 0;
            var z=1;

            z++;



            $('#add').click(function () {
                i++;
                $('#dynamic_field').append(

                    '<tr id="row' + i + '" class="dynamic-added">' +
                    '<td> <select name="barang_id[]" class="form-control" id="barang_id' + i + '">\n' +
                        '<option>Pilih Barang</option>'+
                        @foreach($barang as $key =>$item)
                                '<option value="{{$item->id}}">{{$item->nama_barang}}-{{$item->harga_satuan}}</option>'+
                        @endforeach

                    '                                    </select></td>' +
                    '' +
                    '<td><input type="text" name="harga[]"  class="form-control harga" id="harga' + i + '" readonly></td>' +
                    '<td><input type="number"  name="jumlah[]" min="1"  class="form-control jumlah" id="jumlah' + i + '"></td>' +
                    '<td><input type="text" name="subTotal[]" readonly class="form-control subTotal" id="subTotal' + i + '" /></td>' +
                    '<td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td>' +
                    '</tr>'
                );

                $("#barang_id" + i).on('change', function () {
                    var selectVal = $("#barang_id" + i + " option:selected").val();
                    $.ajax({
                        type: "GET",
                        url: "{{url('/backend/master/barang/get-data')}}",
                        dataType: 'json',
                        data: {
                            id: selectVal,
                        },
                        success: function (data) {
                            console.log(data);
                            $("#harga" + i).val(data.harga_satuan);

                        },
                        error: function (e) {
                            console.log(e.message);
                        }
                    });

                });

                $("table").on("change", "input", function() {
                    var row = $(this).closest("tr");
                    var qty = parseFloat(row.find(".jumlah").val());
                    var price = parseFloat(row.find(".harga").val());
                    var tcost = qty * price;
                    row.find(".subTotal").val(isNaN(tcost) ? "" : tcost);
                    var totalValue=0;
                    $(document).find(".subTotal").each(function(){
                        totalValue += parseFloat($(this).val());
                    });
                    $("#total").val(totalValue);

                });

                $(document).on('click', '.btn_remove', function () {
                    var id = $(this).attr("id");
                    $('#row' + id + '').remove();
                    var row = $(this).closest("tr");
                    var qty = parseFloat(row.find(".jumlah").val());
                    var price = parseFloat(row.find(".harga").val());

                    var tcost = qty * price;
                    row.find(".subTotal").val(isNaN(tcost) ? "" : tcost);
                    var totalValue=0;
                    $(document).find(".subTotal").each(function(){
                        totalValue += parseFloat($(this).val());
                    });
                    $("#total").val(totalValue);


                });

            });
        });




    </script>
@endsection
