@extends('layout.main')
@section('title', $title)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Report</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Report</a>
                </li>


                <li class="active">
                    <a>Mutasi Stock</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <form method="POST" action="" onsubmit="return false" id="form-konten" class="form-horizontal">


                                <div class='form-group'>
                                    <label class="col-sm-2 control-label">Tanggal Awal</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="tanggal_awal" class="form-control" required>
                                        </div>
                                    </div>
                                    <br>

                                </div>
                                <div class='form-group'>
                                    <label class="col-sm-2 control-label">Tanggal Akhir</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="tanggal_akhir" class="form-control" required>
                                        </div>
                                    </div>

                                </div>
                                <br>

                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 col-xs-12 control-label">Barang</label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select name="barang_id" class="form-control">
                                                @foreach($barang as $num => $period)
                                                    <option value="{{$period->id}}">{{$period->nama_barang}}</option>
                                                @endforeach
                                            </select>
                                            <br>
                                        </div>
                                    </div>


                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <label for="name" class="col-sm-2 col-xs-12 control-label"></label>
                                    <div class="form-group">
                                        <div class="col-sm-8 col-xs-12">
                                            <input type="submit" class="btn btn-primary" value="Tampilkan Data">
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <br><br>
                            <div style="clear: both"></div>
                            <div id="results"></div>

                        </div>


                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <br />

    <div class="row">
        &nbsp;
    </div>
@section('scripts')
    <script>
        $("#form-konten").submit(function () {
            var data = getFormData("form-konten");
            ajaxTransfer("/backend/report/mutasi-stock/show", data, function (result) {
                $("#results").html(result);
                $("#table-data").dataTable();
            });
        })
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            startDate: "today",
            format: "yyyy-mm-dd"
        })
    </script>
@endsection
@endsection

