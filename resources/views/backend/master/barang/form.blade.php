<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama Barang</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="nama_barang" class="form-control" value="{{$data->nama_barang}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Diskripsi</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="deskripsi_barang" class="form-control" value="{{$data->deskripsi_barang}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Harga Satuan</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="number" name="harga_satuan" class="form-control" value="{{$data->harga_satuan}}" step=".01" required="">
        </div>
    </div>

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Stock</label>
        <div class='col-sm-9 col-xs-12'>
            <input type="number" name="stock" class="form-control" value="{{$data->stock}}" required="">
        </div>
    </div>




    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/barang/save', data, '#result-form-konten');
        })
    })
</script>
