<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.00
 */

namespace App\Helpers;


use App\Models\Barang;
use App\Models\Pelanggan;
use App\Models\Pembelian;
use App\Models\Penjualan;
use App\Models\Supplier;
use App\Models\Menu;
use App\Models\RoleMenu;

class Helper
{

    public  static  function generateKodePelanggan()
    {
        $cek=Pelanggan::latest()->first();

        if(!is_null($cek)){
            $kode = $cek->kode_pelanggan;
            $noUrut = (int) substr($kode, 3, 3);
            $noUrut++;
            $char = "P";
            $kodeGenerate = $char . sprintf("%03s", $noUrut);
        }else{
            $kodeGenerate ="P001";
        }

        return $kodeGenerate;


    }

    public  static  function generateKodeSupplier()
    {
        $cek=Supplier::latest()->first();

        if(!is_null($cek)){
            $kode = $cek->kode_supplier;
            $noUrut = (int) substr($kode, 3, 3);
            $noUrut++;
            $char = "S";
            $kodeGenerate = $char . sprintf("%03s", $noUrut);
        }else{
            $kodeGenerate ="S001";
        }

        return $kodeGenerate;


    }

    public  static  function generateKodeBarang()
    {
        $cek=Barang::latest()->first();

        if(!is_null($cek)){
            $kode = $cek->kode_barang;
            $noUrut = (int) substr($kode, 3, 3);
            $noUrut++;
            $char = "B";
            $kodeGenerate = $char . sprintf("%03s", $noUrut);
        }else{
            $kodeGenerate ="B001";
        }

        return $kodeGenerate;


    }

    public  static  function generateKodePembelian()
    {
        $cek=Pembelian::latest()->first();

        if(!is_null($cek)){
            $kode = $cek->kode_pembelian;
            $noUrut = (int) substr($kode, 3, 3);
            $noUrut++;
            $char = "TB";
            $kodeGenerate = $char . sprintf("%03s", $noUrut);
        }else{
            $kodeGenerate ="TB001";
        }

        return $kodeGenerate;


    }

    public  static  function generateKodePenjualan()
    {
        $cek=Penjualan::latest()->first();

        if(!is_null($cek)){
            $kode = $cek->kode_penjualan;
            $noUrut = (int) substr($kode, 3, 3);
            $noUrut++;
            $char = "TJ";
            $kodeGenerate = $char . sprintf("%03s", $noUrut);
        }else{
            $kodeGenerate ="TJ001";
        }

        return $kodeGenerate;
    }

    public  static function sessionRole()
    {
        if(session()->exists('activeUser')){
            return request()->session()->get('activeUser')->role_id;
        }
    }



    public static function menuAllowTo($parentId=0,$arrayParent=array())
    {
        $menuRole=RoleMenu::where(['role_id'=>self::sessionRole()])->get();
        $idMenuRole=$menuRole->pluck('menu_id');
        $recursiveMenu=Menu::whereIn('id',$idMenuRole->all())->get();
        if($parentId==0)
        {
            foreach ($recursiveMenu as $item){
                if(($item->parent_id != 0) && !in_array($item->parent_id,$arrayParent)){
                    $arrayParent[] = $item->parent_id;
                }

            }
        }


        $html = "";
        foreach($recursiveMenu as $menu)
        {
            if($menu->parent_id==$parentId)
            {
                $menuUrl=url("$menu->url");
                if(in_array($menu->id,$arrayParent))
                {
                    $html .= "<li>";
                    $html .= "<a href='$menuUrl'> <i class='fa fa-circle-o'></i><span class='nav-label'>$menu->name </span><span class='fa arrow'></span></a>";
                }
                else {
                    $html .= '<li>';
                    $html .= "<a href='$menuUrl'><i class='fa fa-circle-o'></i> <span class='nav-label'>$menu->name</span> </a>";
                }
                if(in_array($menu->id,$arrayParent))
                {
                    $html .= "<ul class='nav nav-second-level collapse'> ";
                    $html .= self::menuAllowTo($menu->id,$arrayParent);
                    $html .= '</ul>';
                }
                $html .= '</li>';
            }
        }
        return $html;



    }




}