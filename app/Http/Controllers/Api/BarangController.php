<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Http\Controllers\Api;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public  function getAll(){

        $data=Barang::all();

        return ($data->count()>0)? response()->json([
            'code'=>200,
            'message'=>'Get Data Successfully',
            'data'=>$data
        ]):
        response()->json([
            'code'=>404,
            'message'=>'Data not found',
            'data'=>[]
        ]);

    

    }

    public  function getOne($id){

        $data=Barang::find($id);

        return (!is_null($data))? response()->json([
            'code'=>200,
            'message'=>'Get Data Successfully',
            'data'=>$data
        ]):
        response()->json([
            'code'=>404,
            'message'=>'Data not found',
            'data'=>[]
        ]);

    

    }
    public  function  create(Request $request){
       
        $data = new Barang();
        $data->kode_barang=$request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->deskripsi_barang=$request->deskripsi_barang;
        $data->harga_satuan=$request->harga_satuan;
        $data->stock=$request->stock;


        try{
            $data->save();
            return response()->json([
                'code'=>200,
                'message'=>'Create data succesfull',
                'data'=>[]
            ]);
           
        } catch (\Exception $ex){

            return response()->json([
                'code'=>404,
                'message'=>'Create data not succesfully',
                'data'=>[]
            ]);
        }

    }

    public  function  update(Request $request,$id){
       
        $data = Barang::find($id);
        $data->kode_barang=$request->kode_barang;
        $data->nama_barang = $request->nama_barang;
        $data->deskripsi_barang=$request->deskripsi_barang;
        $data->harga_satuan=$request->harga_satuan;
        $data->stock=$request->stock;


        try{
            $data->save();
            return response()->json([
                'code'=>200,
                'message'=>'Update data succesfull',
                'data'=>[]
            ]);
           
        } catch (\Exception $ex){
            return response()->json([
                'code'=>404,
                'message'=>'Update data not succesfully',
                'data'=>[]
            ]);
        }

    }
    public  function  delete($id){

       try{
           Barang::find($id)->delete();
           return response()->json([
            'code'=>200,
            'message'=>'Delete data succesfull',
            'data'=>[]
        ]);
       }catch(\Exception $e)
       {
        return response()->json([
            'code'=>404,
            'message'=>'Delete data not succesfully',
            'data'=>[]
        ]);

       }

    }

   
}