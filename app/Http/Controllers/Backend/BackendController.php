<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/04/18
 * Time: 21.33
 */

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Helpers\Helper;
use App\Models\RoleMenu;
use App\Models\Permission;

class BackendController extends Controller {

    public function index(Request $request)
    {
        return view('backend.dashboard.index');
    }


    public function menu(Request $request)
    {
        $menu=Menu::where(['url'=>$request->get('url')])->first();
            if($menu)
            {
                $checkMenuByRole=RoleMenu::where([
                    'menu_id'=>$menu->id,
                    'role_id'=>Helper::sessionRole()
                ])->first();

                if($checkMenuByRole)
                {
                    $permission=Permission::all();
                    $roleMenuPermission=$checkMenuByRole->getRoleMenuPermission;
                    $permissionData=[];
                    foreach($permission as $key => $item)
                    {

                        if(isset($roleMenuPermission[$key]['permission_id'])==$item->id){
                            $show=true;
                            
                        }else{
                            $show=false;
                        }

                        $permissionData[]=[
                            'id'=>$item->id,
                            'permission_name'=>$item->alias,
                            'is_show'=>$show
                        ];
                    
                    }

                    return response()->json([
                        'code'=>200,
                        'message'=>'Ambil data berhasil',
                        'data'=>$permissionData
                    ]);
                    
                }else
                {

                    return response()->json([
                        'code'=>404,
                        'message'=>'Ambil data tidak berhasil',
                        'data'=>[]
                    ]);
                }
                
            }else{

                return response()->json([
                    'code'=>404,
                    'message'=>'Data tidak ada',
                    'data'=>[]
                ]);
            }
    }
}