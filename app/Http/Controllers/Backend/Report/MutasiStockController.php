<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 13.11
 */

namespace App\Http\Controllers\Backend\Report;


use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\Pembelian;
use App\Models\Penjualan;
use Illuminate\Http\Request;


class MutasiStockController extends Controller
{

    public  function index(){

        $params=[
            'barang'=>Barang::all(),
            'title'=>'Manajemen Mutasi Stok'
        ];

        return view('backend.report.mutasi-stock.index',$params);

    }

    public function show(Request $request){



        try{
            $tanggal_awal=$request->tanggal_awal;
            $tanggal_akhir=$request->tanggal_akhir;
            $barang_id=$request->barang_id;

            if(is_null($tanggal_awal)){
                return "<div class='alert alert-danger'>Tanggal awal tidak boleh kosong!</div>";

            }

            if(is_null($tanggal_akhir)){
                return "<div class='alert alert-danger'>Tanggal akhir tidak boleh kosong!</div>";

            }
            $stockMasuk=Pembelian::with('getDetail',function ($query) use($barang_id){
                $query->where('barang_id',$barang_id)->get();
            })
                ->whereBetween('tanggal_pembelian', [$tanggal_awal, $tanggal_akhir])
                ->selectRaw('sum(jumlah)')
                ->selectRaw('tanggal_pembelian')
                ->groupBy('tanggal_pembelian')
                ->get();
            $stockKeluar=Penjualan::with('getDetail',function ($query) use($barang_id){
                $query->where('barang_id',$barang_id)->get();
            })->whereBetween('tanggal_penjualan', [$tanggal_awal, $tanggal_akhir])
                ->selectRaw('sum(jumlah)')
                ->selectRaw('tanggal_penjualan')
                ->groupBy('tanggal_penjualan')
                ->get();

            $params=[
                'dataMasuk'=>$stockMasuk,
                'dataKeluar'=>$stockKeluar

            ];
            return view('backend.report.mutasi-stock.result',$params);
        }catch (\Exception $e){
            dd($e->getMessage());
        }


    }

}