<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 10.58
 */

namespace App\Http\Controllers\Backend\Master;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{

    public  function index(){

        $data=Supplier::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Supplier'
        ];

        return view('backend.master.supplier.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Supplier::find($id);
        }else{
            $data = new Supplier();
        }
        $params = [
            'title' => 'Manajemen Supplier',
            'data' => $data
        ];
        return view('backend.master.supplier.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Supplier::find($id);
        }else {
            $data = new Supplier();
            $data->kode_supplier=Helper::generateKodeSupplier();
        }

        $data->nama_supplier = $request->nama_supplier;
        $data->no_telp=$request->no_telp;
        $data->alamat=$request->alamat;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            Supplier::find($id)->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal dihapus!</div>";
        }

    }


}