<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Http\Controllers\Backend\Master;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public  function index(){

        $data=Barang::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Barang'
        ];

        return view('backend.master.barang.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Barang::find($id);
        }else{
            $data = new Barang();
        }
        $params = [
            'title' => 'Manajemen Barang',
            'data' => $data
        ];
        return view('backend.master.barang.form',$params);
    }
    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Barang::find($id);
        }else {
            $data = new Barang();
            $data->kode_barang=Helper::generateKodeBarang();

        }

        $data->nama_barang = $request->nama_barang;
        $data->deskripsi_barang=$request->deskripsi_barang;
        $data->harga_satuan=$request->harga_satuan;
        $data->stock=$request->stock;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal disimpan!</div>";
        }

    }
    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            Pelanggan::find($id)->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal dihapus!</div>";
        }

    }

    public function getData(Request $request)
    {
        $id=$request->id;
        $data = Barang::find($id);

        return response()->json($data);

    }
}