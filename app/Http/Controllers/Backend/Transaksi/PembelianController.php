<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.46
 */

namespace App\Http\Controllers\Backend\Transaksi;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\DetailPembelian;
use App\Models\Pembelian;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PembelianController extends Controller
{

    public  function index(){

        $data=Pembelian::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Pembelian'
        ];

        return view('backend.transaksi.pembelian.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Pembelian::find($id);
            $title='Edit Pembelian';
        }else{
            $data = new Pembelian();
            $title='Tambah Pembelian';
        }
        $params = [
            'title' => $title,
            'data' => $data,
            'supplier'=>Supplier::all(),
            'barang'=>Barang::all()
        ];
        return view('backend.transaksi.pembelian.form',$params);
    }


    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Pembelian::find($id);
        }else {
            $data = new Pembelian();
            $data->kode_pembelian=Helper::generateKodePembelian();

        }

        $data->tanggal_pembelian = $request->tanggal_pembelian;
        $data->supplier_id=$request->supplier_id;
        $data->total_biaya=$request->total_biaya;
        $data->created_by_user_id=Session::get('activeUser')->id;


        try{
            $barangId=$request->barang_id;
            $harga=$request->harga;
            $jumlah=$request->jumlah;
            DB::beginTransaction();
            $data->save();
            foreach ($barangId as $key =>$item){
                $cekStock=Barang::find($barangId[$key]);
                $cekStock->stock+=$jumlah[$key];
                $cekStock->save();
                $newDetail=new DetailPembelian();
                $newDetail->barang_id=$barangId[$key];
                $newDetail->harga_satuan=$harga[$key];
                $newDetail->pembelian_id=$data->id;
                $newDetail->jumlah=$jumlah[$key];
                $newDetail->save();
                DB::commit();
            }
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); pindah() </script>";
        } catch (\Exception $ex){
            DB::rollBack();
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal disimpan!</div>";
        }

    }

    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            $data=Pembelian::find($id);
            $data->getDetail()->delete();
            $data->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal dihapus!</div>";
        }

    }




}