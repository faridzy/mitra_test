<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 12.45
 */

namespace App\Http\Controllers\Backend\Transaksi;


use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\DetailPenjualan;
use App\Models\Pelanggan;
use App\Models\Penjualan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class PenjualanController extends Controller
{
    public  function index(){

        $data=Penjualan::all();
        $params=[
            'data'=>$data,
            'title'=>'Manajemen Penjualan'
        ];

        return view('backend.transaksi.penjualan.index',$params);

    }

    public  function form(Request $request){

        $id = $request->input('id');
        if($id){
            $data = Penjualan::find($id);
            $title='Edit Penjualan';
        }else{
            $data = new Penjualan();
            $title='Tambah Pembelian';
        }
        $params = [
            'title' => $title,
            'data' => $data,
            'pelanggan'=>Pelanggan::all(),
            'barang'=>Barang::all()
        ];
        return view('backend.transaksi.penjualan.form',$params);
    }


    public  function  save(Request $request){
        $id = intval($request->input('id', 0));
        if($id){
            $data = Penjualan::find($id);
        }else {
            $data = new Penjualan();
            $data->kode_penjualan=Helper::generateKodePenjualan();

        }

        $data->tanggal_penjualan = $request->tanggal_penjualan;
        $data->pelanggan_id=$request->pelanggan_id;
        $data->total_biaya=$request->total_biaya;
        $data->created_by_user_id=Session::get('activeUser')->id;


        try{
            $barangId=$request->barang_id;
            $harga=$request->harga;
            $jumlah=$request->jumlah;
            DB::beginTransaction();
            $data->save();
            foreach ($barangId as $key =>$item){
                $cekStock=Barang::find($barangId[$key]);
                if($cekStock->stock<5){
                    return "<div class='alert alert-danger'>Stock tidak cukup!</div>";

                }
                $cekStock->stock-=$jumlah[$key];
                $cekStock->save();
                $newDetail=new DetailPenjualan();
                $newDetail->barang_id=$barangId[$key];
                $newDetail->harga_satuan=$harga[$key];
                $newDetail->penjualan_id=$data->id;
                $newDetail->jumlah=$jumlah[$key];
                $newDetail->save();
                DB::commit();
            }
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop();  pindah();</script>";
        } catch (\Exception $ex){
            DB::rollBack();
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal disimpan!</div>";
        }

    }

    public  function  delete(Request $request){

        $id = intval($request->input('id', 0));
        try{
            $data=Penjualan::find($id);
            $data->getDetail()->delete();
            $data->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Data gagal dihapus!</div>";
        }

    }

}