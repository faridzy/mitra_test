<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/04/18
 * Time: 21.39
 */

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\RoleMenu;
use App\Models\Menu;

class VerifyMenu
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        if (empty(session('activeUser'))) {
            return redirect('/login');
        } else {
            $menu=Menu::where(['url'=>$request->getPathInfo()])->first();
            if($menu)
            {
                $checkMenuByRole=RoleMenu::where([
                    'menu_id'=>$menu->id,
                    'role_id'=>Helper::sessionRole()
                ])->first();
                if(!$checkMenuByRole)
                {
                    return redirect("/backend");
                }else
                {
                    return $next($request);
                }
            
            }else
            {
                return redirect("/backend");
            }
            
        }
    }
}