<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DetailPembelian extends Model
{
    protected $table = 'detail_pembelian';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable=[
        'barang_id',
        'pembelian_id',
        'harga_satuan',
        'jumlah'
    ];

    public function getBarang()
    {
        return $this->hasOne('App\Models\Barang','id','barang_id');
    }

    public function getPembelian()
    {
        return $this->hasOne('App\Models\Pembelian','id','pembelian_id');
    }
}