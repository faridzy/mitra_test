<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 12.46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable=[
        'kode_penjualan',
        'tanggal_penjualan',
        'total_biaya',
        'pelanggan_id',
        'created_by_user_id'
    ];

    public function getPelanggan()
    {
        return $this->hasOne('App\Models\Pelanggan','id','pelanggan_id');
    }

    public function getUser()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

    public function getDetail()
    {
        return $this->hasMany('App\Models\DetailPenjualan','penjualan_id','id');
    }

}