<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 12.46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DetailPenjualan extends Model
{
    protected $table = 'detail_penjualan';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable=[
        'barang_id',
        'penjualan_id',
        'harga_satuan',
        'jumlah'
    ];

    public function getBarang()
    {
        return $this->hasOne('App\Models\Barang','id','barang_id');
    }

    public function getPenjualan()
    {
        return $this->hasOne('App\Models\Penjualan','id','penjualan_id');
    }
}