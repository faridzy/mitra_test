<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RoleMenuPermission extends Model
{
    protected $table = 'role_menu_permission';
    protected $primaryKey = 'id';
    public $timestamp=false;

    public function getPermission()
    {
        return $this->hasOne('App\Models\Permission','id','permission_id');
    }
}