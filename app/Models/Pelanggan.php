<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 10.57
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'pelanggan';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable=[
        'kode_pelanggan',
        'nama_pelanggan',
        'no_telp',
        'alamat'
    ];

}