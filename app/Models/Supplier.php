<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 10.55
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Supplier extends  Model
{
    protected $table = 'supplier';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable=[
        'kode_supplier',
        'nama_supplier',
        'no_telp',
        'alamat'
    ];





}