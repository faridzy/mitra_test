<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable=[
        'kode_barang',
        'nama_barang',
        'deskripsi_barang',
        'harga_satuan',
        'stock'
    ];

}