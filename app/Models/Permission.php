<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permission';
    protected $primaryKey = 'id';
    public $timestamp=false;
    
}