<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menu';
    protected $primaryKey = 'id';
    public $timestamp=false;


    public function getMenu()
    {
        return $this->hasOne('App\Models\Menu','id','menu_id');
    }

    public function getRole()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }

    public function getRoleMenuPermission()
    {
        return $this->hasMany('App\Models\RoleMenuPermission','role_menu_id','id');
    }

}