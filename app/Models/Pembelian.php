<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{

    protected $table = 'pembelian';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable=[
        'kode_pembelian',
        'tanggal_pembelian',
        'total_biaya',
        'supplier_id',
        'created_by_user_id'
    ];

    public function getSupplier()
    {
        return $this->hasOne('App\Models\Supplier','id','supplier_id');
    }

    public function getUser()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

    public function getDetail()
    {
        return $this->hasMany('App\Models\DetailPembelian','pembelian_id','id');
    }

}