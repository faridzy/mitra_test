<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 18/05/19
 * Time: 11.27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id';
    public $timestamp=false;

}